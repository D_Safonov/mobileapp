﻿using UnityEngine;
using System.Collections;
using System;

public class AndroidWiFiConnector : MonoBehaviour {

	void Start()
	{
		ShowWiFiScreen ();
	}


	public void ShowWiFiScreen()
	{
		#if UNITY_ANDROID
		AndroidJavaClass activityClass = new AndroidJavaClass("com.example.wificonnector.MainActivity");
		AndroidJavaClass unityActivityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject activityObj = unityActivityClass.GetStatic<AndroidJavaObject>("currentActivity");
		#else
		 AndroidJavaClass unityActivityClass = null;
		AndroidJavaClass	activityClass = null;
		AndroidJavaObject activityObj = null;
		#endif
		if (activityObj != null)
		{
			Debug.LogWarning("ShowWiFiScreen");
			activityObj.Call ("OpenWiFiSettings");
		}
	}
}