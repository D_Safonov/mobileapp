﻿using UnityEngine;
using System.Collections;

public static class JsonBuilder
{
	private static string result;
	private static string formatStr =  "{{\"method\":\"getUser\",\"name\":\"{0}\",\"pass\":\"{1}\"}}";
	public static string BuildGetUser(string login, string password)
	{
		result = string.Format(formatStr,login,password);
		return result;
	}
}
