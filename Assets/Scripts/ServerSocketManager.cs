﻿using UnityEngine;
using System.Collections;

public class ServerSocketManager : MonoBehaviour {

	private static string serverURL =  "http://webbears.in.ua/test/index.php";
	// Use this for initialization
	void Start () {
		//WiFiConnectorNamespace.WiFiConnector connector = new WiFiConnectorNamespace.WiFiConnector();
		//connector.GetConnectionWithName("MyNetwork");
		//Debug.Log( UnityPluginForWindowsPhone.Class1.GetDeviceName );
		StartCoroutine( LoginToServer("k3yart","windows"));
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public IEnumerator LoginToServer(string login, string password)
	{
		WWWForm parameters = new WWWForm();
		parameters.AddField("method",JsonBuilder.BuildGetUser(login,password));
		WWW request = new WWW(serverURL, parameters);

		yield return request;
		if (request.error != null)
		{
			Debug.Log("request error: " + request.error);
		}
		else
		{
			Debug.Log("request success");
			Debug.Log("returned data" + request.text);
		}
	}
}
