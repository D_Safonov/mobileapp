package com.example.wificonnector;

import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;

import com.unity3d.player.UnityPlayerActivity;

public class MainActivity extends UnityPlayerActivity
{
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		OpenWiFiSettings();
		super.onCreate(savedInstanceState);
	}
	
	public void OpenWiFiSettings()
	{
		//setContentView(R.layout.activity_main);
		startActivity(new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK));
	}
}
