package com.example.wificonnector;

import java.lang.System;
import java.util.List;

import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;

public class ConnectManager extends MainActivity 
{
	//Class member ------------------------------------------------------
	WifiManager m_pWiFiManager;
	WifiConfiguration m_WiFiConfig;
	String m_sNetworkSSID = "COFFEE_POINT";
	
	// Methods ----------------------------------------------------------
	public ConnectManager() 
	{
		m_WiFiConfig = new WifiConfiguration();
	}
	
	public void Init()
	{
		System.out.println( "\nCreation WiFi configuration...\n" );
		
		m_WiFiConfig.SSID = "\"" + m_sNetworkSSID + "\"";
		m_WiFiConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);

		m_pWiFiManager = (WifiManager)getSystemService(WIFI_SERVICE); 
		m_pWiFiManager.addNetwork(m_WiFiConfig);
		
		ConnectToWiFiPoint();
	}
	
	public void SetPointName(String _pointName)
	{
		m_sNetworkSSID = _pointName;
	}
	
	private void ConnectToWiFiPoint()
	{
		System.out.println( "\nFind WiFi point with SSID " + m_sNetworkSSID + "...\n" );
		
		List<WifiConfiguration> networksList = m_pWiFiManager.getConfiguredNetworks();
		
		for( WifiConfiguration i : networksList ) 
		{
		    if( i.SSID != null && i.SSID.equals("\"" + m_sNetworkSSID + "\"") ) 
		    {
		    	System.out.println( "\nConnecting to " + m_sNetworkSSID + "...\n" );
		    	
		    	m_pWiFiManager.disconnect();
		    	m_pWiFiManager.enableNetwork(i.networkId, true);
		    	m_pWiFiManager.reconnect();               
		    	
		    	return;
			}           
		}
		
		System.out.println( "\nWiFi point with SSID " + m_sNetworkSSID + " not found!\n" );
	}

}
